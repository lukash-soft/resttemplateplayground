package com.example.MovieRestApi.featureFacts;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@Service
public class FactService {

    private final RestTemplate restTemplate;
    private final String path;

    public FactService(RestTemplate restTemplate,
                       @Value("${api.facts}") String path) {
        this.restTemplate = restTemplate;
        this.path = path;
    }

    public List<FactModel> getFacts(String animal, int numberOfFacts) {
        final String url = path + "facts/random?animal_type=" + animal + "&amount=" + numberOfFacts;

        HttpEntity<FactModel[]> httpEntity = restTemplate.getForEntity(url, FactModel[].class);

        return Arrays.asList(httpEntity.getBody());
    }

    public FactModel getFactById(Long id){
        final String url = path + "facts/" + id;
        HttpEntity<FactModel> httpEntity = restTemplate.getForEntity(url, FactModel.class);

        return httpEntity.getBody();
    }
}
