package com.example.MovieRestApi.featureFacts;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("cats")
@RequiredArgsConstructor
public class FactController {

    private final FactService factService;

    @GetMapping("facts")
    public List<FactModel> getRandomFact(@RequestParam String animal,
                                         @RequestParam int numberOfFacts){
            return factService.getFacts(animal, numberOfFacts);
    }

    @GetMapping("/getById/{factId}")
    public FactModel getById(@PathVariable("factId") Long factId){
        return factService.getFactById(factId);
    }

}
